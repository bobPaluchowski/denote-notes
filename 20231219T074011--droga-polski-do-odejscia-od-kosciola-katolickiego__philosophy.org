#+title:      Droga Polski do odejscia od kosciola katolickiego
#+date:       [2023-12-19 Tue 07:40]
#+filetags:   :philosophy:
#+identifier: 20231219T074011

* Zarys sytuacji politycznej w Polsce.

* Dlaczego kosciol katolicki ma silna pozycje w Polsce?

* Polska A i Polska B w walce politycznej.

* Czy duze miasta utrzymaja wzrostowy trend swieckigo spoleczenstwa?

* Dekomunizacja a umocnienie sie kosciola katolickiego w Polsce.

* Wspolna Europa i strach przed utrata tozsamosci narodowej.
